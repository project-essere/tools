import csv
import fileinput
import re
import sys

srcPath = sys.argv[1]
dstPath = sys.argv[2]

reVersion = re.compile(r"Analysing version '(?P<data>\S+)'")
reFilesCount = re.compile(r"Source files count: (?P<data>\d+)")
reCD = re.compile(r"CD detector found (?P<data>\d+)")
reGC = re.compile(r"GC detector found (?P<data>\d+)")
reHL = re.compile(r"HL detector found (?P<data>\d+)")
reUD = re.compile(r"UD detector found (?P<data>\d+)")


with open(dstPath, 'w', newline='') as csvFile:
    writer = csv.writer(csvFile)
    writer.writerow(['version', 'files', 'cd', 'gc', 'hl', 'ud'])

    data = []

    with fileinput.input(files=(srcPath)) as src:
        for line in src:
            match = reVersion.search(line)
            if match is not None:
                version = match.groupdict()['data']
                data.append(version)

            match = reFilesCount.search(line)
            if match is not None:
                files = match.groupdict()['data']
                data.append(files)

            match = reCD.search(line)
            if match is not None:
                cd = match.groupdict()['data']
                data.append(cd)

            match = reGC.search(line)
            if match is not None:
                gc = match.groupdict()['data']
                data.append(gc)

            match = reHL.search(line)
            if match is not None:
                hl = match.groupdict()['data']
                data.append(hl)

            match = reUD.search(line)
            if match is not None:
                ud = match.groupdict()['data']
                data.append(ud)

                writer.writerow(data)
                data.clear()



