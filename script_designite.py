import csv
import fileinput
import re
import sys

srcPath = sys.argv[1]
dstPath = sys.argv[2]

regexes = [
    re.compile(r"Analysing version '(\S+)'"),

    # Summary.
    re.compile(r"Total LOC analyzed: (\d+)\s+Number of packages: (\d+)"),
    re.compile(r"Number of classes: (\d+)\s+Number of methods: (\d+)"),

    # Architecture smell.
    re.compile(r"Cyclic dependency: (\d+)\s+God component: (\d+)"),
    re.compile(r"Ambiguous interface: (\d+)\s+Feature concentration: (\d+)"),
    re.compile(r"Unstable dependency: (\d+)\s+Scattered functionality: (\d+)"),
    re.compile(r"Dense structure: (\d+)")
]

# CSV fields.
fields = ['version',
    'lines of code',
    'number of packages',
    'number of classes',
    'number of methods',
    'cyclic dependency',
    'god component',
    'ambiguous interface',
    'feature concentration',
    'unstable dependency',
    'scattered functionality',
    'dense structure'
]

with open(dstPath, 'w', newline='') as csvFile:
    writer = csv.writer(csvFile)
    writer.writerow(fields)

    data = []

    with fileinput.input(files=(srcPath)) as src:
        for line in src:
            for regex in regexes:
                match = regex.search(line)
                if match is not None:
                    for res in match.groups():
                        data.append(res)

                    # Row completed, can be written to the CSV file.
                    if len(data) == len(fields):
                        writer.writerow(data)
                        data.clear()

                    # If there is a match skip all other remaining regexes.
                    continue
