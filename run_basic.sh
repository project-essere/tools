#!/usr/bin/env bash

if [[ -z "$(command -v cloc)" ]]; then
  echo "Command cloc not found!"
  exit 1
fi

for project in projects/* ; do
  PROJECT_NAME=$(basename $project)

  printf "Analysed project path: $project\t Name: $PROJECT_NAME\n"

  counter=0

  BASE_DIR=output/baseOutput/
  OUTPUT="$BASE_DIR/$PROJECT_NAME.csv"

  mkdir -p "$BASE_DIR"

  printf "version, commit_hash, raw_tag, date, loc\n" > "$OUTPUT"

  # Analyse this project for each specified commit.
  while read commit; do
    git -C $project checkout $commit

    counter=$((counter+1))
    version="V.$counter"

    # We extract only the Java non-comment LOC.
    loc="$(cloc --diff-timeout 20 --quiet --csv "$project" | grep "Java," | cut -d "," -f 5)"

    # We extract git tags (there may be multiple tags) and date.
    data="$(git -C "$project" log -1 --format=format:"%d, %cs" $commit)"

    echo "$version, $commit,$data, $loc" >> "$OUTPUT"
  done < "commits_$PROJECT_NAME.txt"
done
