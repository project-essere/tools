#!/usr/bin/env bash

if [[ -z "$(command -v cloc)" ]]; then
  echo "Command cloc not found!"
  exit 1
fi

PROJECT=projects/flume
PROJECT_NAME=flume

printf "Analysed project path: $PROJECT\t Name: $PROJECT_NAME\n"

BASE_DIR=output/manualOutput/
OUTPUT="$BASE_DIR/${PROJECT_NAME}_channel_file_god_component.csv"

mkdir -p "$BASE_DIR"

printf "version,cloc_classes,cloc_loc,pseudo_designite_classes\n" > "$OUTPUT"

counter=0

# Analyse this project for each specified commit.
while read commit; do
  git -C $PROJECT checkout $commit

  counter=$((counter+1))
  version="V.$counter"

  data="$(find . -regex '.*/apache/flume/channel/file/\w*\.java' | xargs cloc --diff-timeout 20 --quiet --csv | grep 'Java,' | cut -d ',' -f 1,5)"

  files="$(echo $data | cut -d ',' -f 1)"
  loc="$(echo $data | cut -d ',' -f 2)"
  designite_files="$(find . -regex '.*/apache/flume/channel/file/.*' | wc -l)"

  echo "$version,$files,$loc,$designite_files" >> "$OUTPUT"
done < "commits_$PROJECT_NAME.txt"
