#!/usr/bin/env bash

ARCAN=${ARCAN:-./arcan.sh}

echo "Arcan executable path: $ARCAN"

for project in projects/* ; do
  echo $project
  echo $(basename $project)

  PROJECT_NAME=$(basename $project)

  echo "$ARCAN analyse -i $project -p $PROJECT_NAME -o output --all -l JAVA --versions commits_$PROJECT_NAME.txt"

  $ARCAN analyse -i "$project" -p "$PROJECT_NAME" -o output --all -l JAVA --versions "commits_$PROJECT_NAME.txt" > out_arcan.log

  python3 script_arcan.py out_arcan.log output/"$PROJECT_NAME"_arcan.csv
done
