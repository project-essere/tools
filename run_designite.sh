#!/usr/bin/env bash

DESIGNITE=${DESIGNITE:-../Designite/DesigniteJava.jar}

echo "Designite JAR path: $DESIGNITE"

for project in projects/* ; do
  PROJECT_NAME=$(basename $project)

  printf "Analysed project path: $project\t Name: $PROJECT_NAME\n"

  counter=0

  BASE_DIR=output/designiteOutput/"$PROJECT_NAME"

  mkdir -p "$BASE_DIR"

  rm -f out_designite.log

  # Analyse this project for each specified commit.
  while read commit; do
    git -C $project checkout $commit

    counter=$((counter+1))
    VERSION="$counter-${commit:0:8}"
    echo "Analysing version '$VERSION'" >> out_designite.log

    OUTPUT_DIR="$BASE_DIR/$VERSION"

    mkdir -p "$OUTPUT_DIR"

    echo "java -jar \"$DESIGNITE\" -i \"$project\" -o \"$OUTPUT_DIR\" >> out_designite.log"
    java -jar "$DESIGNITE" -i $project -o $OUTPUT_DIR >> out_designite.log
  done < "commits_$PROJECT_NAME.txt"

  python3 script_designite.py out_designite.log output/designiteOutput/"$PROJECT_NAME"_designite.csv
done
